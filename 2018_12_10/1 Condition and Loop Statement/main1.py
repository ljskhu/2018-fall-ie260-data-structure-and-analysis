# if elif else: block 얘기할 때 colon
"""
if boolean1 : boolean1이면
elif boolean2 : 앞에 있는 if가 아니고 boolean2가 참이면
else : 기타 경우

python은 switch-case 구문은 없음
"""
numScore = 75

if numScore > 90: print('A')
else: print("Lower grade")

if numScore > 90: print('A')
elif numScore > 80: print('B')
elif numScore > 70: print('C')
else: print("D or F")

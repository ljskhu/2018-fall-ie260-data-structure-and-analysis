"""
for
for variable in sequence:# sequence가 끝날 때까지 variable을 불러옴
다른 언어에는 거의 없지만
for variable in sequence:
    dsfsfd
else:for loop가 정상적으로 끝났을 때 실행하는 block

continue, break 사용하면 유용
"""

for itr in range(10):#0부터 9까지? 10이 y임 x 기본값은 0임
    print(itr)

sum = 0
for itr in range(1,11):#1부터 10까지
    sum += itr
print(sum)

for itr in range(1,100,10):#1 11 21 ... 91, 51만 출력 안하기 == x:y:z
    if itr == 51:
        continue
    else: print(itr)

for itr in range(5):
    print(itr)
else: print("for loop done")

for itr in range(5):
    if itr == 3:
        break
    print(itr)
else: print("for loop done") # break 되면 else 쪽 block 실행 안된 것 확인할 수 있음
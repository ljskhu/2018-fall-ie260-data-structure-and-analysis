"""
Function Statement
def name(params):
    statements
    return var1, var2 #여러개를 넘길 수 있다. 리턴값의 type과 개수는 자유롭게 가능하다.
즉 y=f(x) 꼴이 아니라 y1, y2 = f(x) 이렇게 R^1에서 R^2로 가는게 가능하다. 다만 순서는 지켜야 함
리턴 타입이 없는 건 장단점이 있음
프로그램 짜기는 편하지만 에러는 나중에 발생함.(미리 잡아내기 어려움)
"""
numA = 1
numB = 2

def add(num1, num2): return num1+num2
def multiply(num1, num2): return num1*2, num2*3
def increase(num1, step = 1): return num1 + step

numC = add(numA, numB)
numD, numE = multiply(numA, numB) # assign되는 쪽도 ,로 구분해서 적어줘야 함
print(multiply(numA,numB)) # 실제로는 tuple로 반환함
print(type(multiply(numA,numB)))

numF = increase(numA, 5)
numG = increase(numA)

"""
one line function 만들기 -> lambda 이용
형식은 아래와 같이 : 뒤에 리턴값
funcname = lambda var1, var2, ... : (리턴값)
엔지니어링 할때는 이렇게 쓰는 경우 많음
"""
lambdaAdd = lambda num1, num2 : num1 + num2
print(type(lambdaAdd)) # <class 'function'>
print(type(add))# <class 'function'>
numH = lambdaAdd(numA, numB)

print(numC, numD, numE, numF, numG, numH) # 3, 2, 6, 6, 2, 3
"""
Finding Prime Numbers
"""
from tqdm import tqdm

#Function for calculation(DEFINITION)
def isPrimeNumber(num):
    if num == 1: return False # 1은 소수가 아니므로 아예 예외처리
    for itr in range(2, num):#2부터 num-1까지 짝수 빼고 반복 #만약 range(2,1)같은 경우에는 빈 리스트를 반환하고  반복할 게 없으므로 바로 else 쪽으로 넘어감
        if num % itr == 0: #나누어 떨어지면 소수가 아님
            break
    else:#한 번도 나누어 떨어지는 경우가 없으면 for문이 종료되고 소수임을 알림
        return True
    return False#break되었으면 소수가 아니란 얘기이므로 false

#Function for iteration(DEFINITION)
def findPrimes (num1, num2):
    numCount = 1
    for itr in tqdm(range(num1, num2)):
        if isPrimeNumber(itr) == True:
            print(str(numCount) + "번째 소수 : " + str(itr))
            numCount += 1
    return 0

findPrimes(1,100000) # FUNCTION CALL

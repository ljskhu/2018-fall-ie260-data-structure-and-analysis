
"""
0 1 2 3 4 5 6 7 8 9 0 1 2 3 4 5 양수
H e l l o   W o r l d !   I S E
6 5 4 3 2 1 0 9 8 7 6 5 4 3 2 1 음수
"""
strTest = "Hello World! ISE"
print(strTest)
strTestComp = "Hello World! ISE"
print(strTestComp, strTest == strTestComp) # strTestComp를 출력하고 이어서 둘이 같은지를 출력

print(strTest[0], strTest[1])#문자열 사이에 , 넣어주면 한 칸 띄고 출력해줌
print(strTest[-1], strTest[-2])#negative index를 사용할 수 있음!

print(len(strTest))
print(strTest+' '+"Dept")#string에서 operator 활용 중
print(strTest*2) #2번 그냥 반복. 곱셈은 반복의 개념으로 해석
print("ISE" in strTest) #문자열이 포함돼 있는지 확인(들어가 있느냐? 참 거짓으로 나옴) in은 인덱스에서도 ok
print("ISE" not in strTest) #문자열이 안 포함돼 있는지 확인

#나중에 데이터가 있는지 없는지 확인하는 데 유용하다!

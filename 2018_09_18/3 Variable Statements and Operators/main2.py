def main():
    numYearBase10 = 2018
    numYearBase8  = 0o3742
    numYearBase16 = 0x7E2

    print("Year by base 10 : %d, "
          "by base 8 : %d, "
          "by base 16 : %d" % (numYearBase10\
    , numYearBase8, numYearBase16))
    """
    %d : 값들을 넣을 수 있도록 
    \(백슬래시) : 줄바꿈했을 때 다음 줄과 이어주는 역할(Soft-Wrap 있으므로 굳이 필요하진 않을 듯.)
    """

    #복소수 선언 2가지 방법
    numComplex1 = complex(3,4)
    numComplex2 = 4+3j

    print("Complex value : ", numComplex1)
    print("Absolute value : ", abs(numComplex2))
    print("Real value : ", numComplex2.real) # 실수부, 허수부 불러올 수 있다. 즉, 인스턴스임.
    print("Image value : ", numComplex2.imag)

    strDeptName = "Enginnering"
    strUnivName = "University"
    print("Department : ", strDeptName) # comma는 연속해서 print할 수 있게 도와줌.
    print("Full name of dept. : ", (strDeptName + ", " + strUnivName))

main()
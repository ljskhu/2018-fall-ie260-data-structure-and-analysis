#Operators 연산자 실습

def main():
    numTest1 = 10
    numTest2 = 3.0
    numPlus = numTest1 + numTest2
    numMinus = numTest1 - numTest2
    numMultiply = numTest1 * numTest2
    numDivide = numTest1 / numTest2
    numModula = numTest1 % numTest2 #나머지

    print("%d, %d, %d, %f, %d" % \
          (numPlus, numMinus, numMultiply, numDivide, numModula))#%d는 decimal이고 %f는 float 의미함!

    """
    type casting -> int(), float(), str()
    """

    numDivideInt = numTest1 / int(numTest2) # 나누기를 쓰면 float으로 바뀌는 모양임. 강좌와 결과 다름.
    print(numDivide, numDivideInt)

    """
    Swapping Statement가 존재해서 굳이 임시변수를 만들어주지 않아도 좋다.
    
    예)
    a, b = b, a
    하면 두 변수의 값이 바뀜
    """
    numTest2, numTest1 = numTest1, numTest2
    print(numTest1, numTest2)

    print(numTest1 == numTest2)
    print(numTest1 != numTest2)
    print(type(numTest1))

    numTest1 = str(numTest1)
    print(type(numTest1), numTest1)

    strFormula = "2018 / 7"
    print(eval(strFormula))



main()
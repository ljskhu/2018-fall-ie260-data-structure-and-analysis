"""
Data Types
있긴 하나 나중에 할당(assign)됨
numOfStudents -> 어떤 type도 없음

1. 첫번째 분류 : Numeric Data Types
integer, float, long integer, octal integer, hexadecimal integer, complex가 있음.

numOfStudents = 10 -> 10이 저장되면 data type이 생김 여기서는 int
integer -> signed integer만

만약
numOfStudents = 10.0 -> float으로. 소수점으로 저장
7.8e-28
같은 형태로도 float 표현 가능

long integer, octal integer, hexadecimal integer, complex는 그냥 숫자로만 쓰면 안 되고 다른 표기를 덧붙여 줘야 함.

long integer 예시 : 234187626348292917L, 7L
뒤에 대문자 L을 붙인다

octal integer : 숫자 앞에 0o을 붙이고시작(python 3에선 0b, 0o, 0x 같은 방식을 사용)
0o100
이라 쓰면 십진수로 64임

hexadecimal integer : 숫자 앞에 0x를 붙이고 시작
0x11
이라 쓰면 십진수로 17임

complex : 실수부는 그대로, 허수는 허수부 뒤에 그냥 j 쓰면 알아서 인식
3+4j
complex(3,4)
이런 식으로



2. 두번째 분류 : String Data Types
문자나 문자열

3. 세번째 분류 : Collection Data Types
Numeric이나 String을 모아둔 것들
list, dictionary, tuple이라는 것들이 존재


"""

def main():
    floatNumber = 7.8e5
    complexNumber = 300454+4j
    print(floatNumber+complexNumber)

main()
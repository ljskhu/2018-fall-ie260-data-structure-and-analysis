#Naming과 Styling
#이름 맘대로 짜면 안 된다!
#val1, val2 같은 이름은 좋지 않다
#회사, 연구실마다 다르지만 규칙은 존재해야 한다

#Naming : 의미를 잘 전달할 수 있는 방식이어야!
#Camel Casing을 기본적으로 한다.
#Camel Casing이란? 낙타 봉우리처럼 적절하게 대문자를 섞어서 사용

#클래스명, 변수명, 메서드명에 사용한다. 주로 의미가 명확한 명사를 사용하는 게 좋다.

#클래스명에선 - 각 단어의 첫 글자를 대문자화 예)MyFirstClass

#변수명에서 - 소문자로 시작하자. 그리고 저장될 내용을 잘 표현하는 명사로 사용하는 게 좋다. 예) numberOfStudents = 100

#메서드명에서 - 기본적으로 동작하는 걸 나타낸 것이므로, 메서드가 수행하는 작업을 동사로 표현하는 게 좋다. 첫 글자는 소문자로 하자. 예) def performAverage(self, val1, val2)

class HelloWorld:
    def __init__(self):
        print("Hello World! Just one more time")
    def __del__(self):
        print("Goodbye!")
    def performAverage(self,val1,val2):
        average = (float(val1) + float(val2))/2.0

def main():
    world = HelloWorld()
    score1, score2 = input("Enter two scores seperated by a comma: ").split(',')
    world.performAverage(score1,score2)

main()
#Procedure Oriented Programming, python 3.7
#POP는 function 기반임
#대응되는 개념은 Object Oriented Programming이고 Class를 만들어서 처리함
#POP에서는 Definition Part와 Execution Part로 구분된다.

#Block 1 Start
def main():
    #def :function declaration keyword
    #main : function name
    #() : input parameter가 없는 경우
    #: : colon을 씀으로써 구역을 선언해 주고 있음

    print("Hello World")

    score1, score2 = input("Enter two scores seperated by a comma: ").split(',') #https://bit.ly/2xmwITG, split() 써야 컴마로 알아서 분리시킴
    average = (float(score1) + float(score2))/2.0 #형변환 필요

    print("The average of the scores is : ", average)
#Block 1 End, 지금 이 block은 함수의 선언에 관련된 부분임

#Block 2 Start
main() #실제 동작시키기 위해서는 꼭 호출해야 함
#Block 2 End
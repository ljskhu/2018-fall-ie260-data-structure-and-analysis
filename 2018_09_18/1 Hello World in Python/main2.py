#Object Oriented Programming, python 3.7
#클래스를 만들어서 선언

#클래스 선언 시작
class HelloWorld:
    #class : class declaration keyword
    #HelloWorld : class name
    #: : colon으로 영역 구분
    def __init__(self):
        #self : 자기 자신을 의미, 클래스의 member function, method로 선언될 경우 self라는 것이 붙게 됨
        print("Hello World! Just one more time")
    def __del__(self):
        print("Goodbye!")
    #__init__:클래스라는 것이 instantiaion이라는 과정을 통해 instance로 바뀌고, instance로 바뀔 때 __init__이 호출됨
    # __del__ : 인스턴스가 사라질 때 호출됨
    # __ 양쪽에 붙는 함수들은 미리 정의된 특수한 함수들. 적절한 시기에 자동으로 호출된다.
    def performAverage(self,val1,val2):
        average = (float(val1)+float(val2))/2.0
        print("The Average of the scores is : ", average)
#클래스 선언 끝

#클래스를 이용하는 걸 함수화하여 코드로 적어둠
def main():
    world = HelloWorld() #헬로월드 객체 하나 생성
    #world : instance storage variable
    #HelloWorld() : instance template(인스턴스를 만들어 내는 놈)
    score1, score2 = input("Enter two scores seperated by a comma : ").split(',')
    #숫자 여러 개를 한 번에 입력 가능하게 해 둠

    world.performAverage(score1,score2)
    #원래 함수 선언부는 input parameter가 3개 있지만 실제로 함수를 쓸 때는 2개만 써도 괜찮다. self는 넣지 않아도 온점 앞에 있는 world의 값을 받아서 전달됨. 즉 메서드 속에서 자기의 인스턴스 접근시 self 키워드 사용 가능하다. 이 예제에서는 활용하고 있지 않음.

#기본적으로 OOP도 Definition Part와 Execution Part로 나뉜다.

main() #앞에서 선언한 실제 main()의 실행부분(trigger함)
"""
list는 대괄호를 이용해서 만듦
"""

lstTest = [1,2,3,4]
print(lstTest) # [1,2,3,4]
print(lstTest[0], lstTest[1], lstTest[2]) #1 2 3
print(lstTest[-1], lstTest[-2]) # 4 3
print(lstTest[1:3]) # [2,3] 마지막 인덱스는 항상 포함 안 된다!!!!
print(lstTest+lstTest) # [1, 2, 3, 4, 1, 2, 3, 4] 더하면 concatenating 됨
print(lstTest*3) #[1, 2, 3, 4, 1, 2, 3, 4, 1, 2, 3, 4] 곱하면 반복됨

lstTest = range(1,20,3) #from x to y by z steps (y는 미포함)
print(lstTest) # [1, 4, 7, 10, 13, 16, 19]

print(4 in lstTest, 100 in lstTest) # 4가 리스트 안에 있는지 확인! True False

lstTest = list(lstTest) # AttributeError: 'range' object has no attribute 'append' 에러 해결하려고 list()사용
lstTest.append("hey")#.는 클래스의 인스턴스 내부에 가는것
print(lstTest) # [1, 4, 7, 10, 13, 16, 19, "hey"] #숫자로만 들어와 있어도 문자열이 들어올 수 있다! 뭐든지 다 저장될 수 있다.

del(lstTest[0])#어떤 리스트를 삭제할 수 있음.
print(lstTest)

lstTest.reverse() #역순정렬
print(lstTest)

lstTest.remove(4)#4라는 요소를 제거(4번째가 아님!)
print(lstTest)

#lstTest.sort() 작동 안 해서 지움 정렬은 오름차순이 기본이라 함 hey도 아스키코드 써서 숫자화한다는데 python3은 안됨
del(lstTest[0])
lstTest = sorted(lstTest) # 숫자만 있거나 문자만 있을 때 가능? 한듯 36번째 줄에서 확인 가능
lstAlphabet = ['B', 'D', 'A', 'C']
lstAlphabet = sorted(lstAlphabet)
print(lstTest, lstAlphabet)

#test
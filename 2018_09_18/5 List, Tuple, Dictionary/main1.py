"""
0 1 2 3 4 5 6 7 8 9 0 1 2 3 4 5 양수
H e l l o   W o r l d !   I S E
7 6 5 4 3 2 1 9 8 7 6 5 4 3 2 1 음수
"""

"""
인덱스 연산 시 [시작:끝:단계]일 떄 끝은 포함이 안됨!
그러니까 시작 이상 끝 미만으로 해석됨.
list, string의 첫 element는 index가 0이다!
 [X:Y]이면 X부터 포함해서 시작해서 Y포함하지 않고 끝남!
그러니까 [X:Y] 의미 : from x to y (y는 절대 포함 안함)
 [X:Y:Z]이면 X부터 포함해서 시작해서 Y포함하지 않고 끝남! 근데 Z개씩 이동
그러니까 [X:Y:Z] 의미 : from x to y with z steps(y는 절대 포함 안함)

len(x) -> x의 길이
ABCD의 길이는 4
다만 인덱스는 0 1 2 3임
"""

strTest = "Hello World! ISE"
print(strTest) #Hello World! ISE
print(strTest[1], strTest[2], strTest[3]) # e l l
print(strTest[1:3]) # el  -> 인덱스의 범위를 지정하는 경우. 인덱스 1, 2만 출력
print(strTest[3:]) # lo World! ISE -> 뒤쪽 범위를 빼면 끝까지란 의미
print(strTest[:3]) # Hel -> 3까지이되 미만 그래서 인덱스 0 1 2 출력
print(strTest[1:9:2]) # el o -> 2개씩 간다. 시작은 1부터 하고 9 포함하지 않고 끝남. 1 3 5 7 츨력
print(strTest[1:len(strTest):2]) # el ol!IE -> 2개씩 간다. 시작은 1부터 하고 끝까지 쭉. 1 3 5 7 9 11 13 15
print(strTest[1::2]) # el ol!IE -> y의 기본값은 the length of the sequence이고 z의 기본값은 1
print(strTest[5::-1])# " olleH" -> 5부터 시작하고 끝까지 가긴 하는데 negative step임. 그러면 5 4 3 2 1 0 출력을 한다. 좌측이든 우측이든 끝이 될 수 있음에 유의한다. 이동 방향에 따라 끝을 왼쪽 혹은 오른쪽으로 생각해야 함.
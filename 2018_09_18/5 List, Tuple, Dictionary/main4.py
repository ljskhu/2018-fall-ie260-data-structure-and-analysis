"""
dictionary : collection variable type
선형적이지 않은 변수타임
key value pair로 저장됨
key:value, key:value 반복하고 감싸주는 괄호는 중괄호
"""

dicTest = {1:"one", 2:"two", 3:"three",}
#주의할 점
"""
리스트나 튜플에서는 lstTest[] tplTest[]에서 []안에 들어가는 값이 index였다
그런데 딕셔너리에서는 dicTest[] []안에 들어가는 값이 key에 해당함
"""
print(dicTest[1]) # one 출력

#새로운 데이터 추가
dicTest[4] = "four" #key는 4로, value는 "four"로
print(dicTest)
dicTest[1] = "hana" #value 변경하기
print(dicTest)


"""
하나의 인스턴스로 가정하고 dictionary class라 생각하면
각 method는 key들만, value들만 모아주고

"""
print(dicTest.keys())
print(type(dicTest.keys())) # class dict_keys
print(dicTest.values())
print(type(dicTest.values())) # class dict_values
print(dicTest.items()) #key value pair를 리스트 형태로 반환함
print(type(dicTest.items())) # class dict_items
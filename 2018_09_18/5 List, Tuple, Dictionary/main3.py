#Tuple
"""
튜플이랑 리스트는 거의 비슷하다
리스트와 다른 점은 변수의 값을 바꿀 때 드러난다
튜플은 리스트와 달리 내용이 바뀌는 것을 허용하지 않음
"""
#tuple def(소괄호를 사용)
tplTest = (1, 2, 3)

print(tplTest) # (1,2,3)
for i in range(0,len(tplTest)): # 1 2 3
    print(tplTest[i])
for j in range(-1,-len(tplTest),-1): # 3 2 1 아니고 3 2 나옴(-1 -2까지만 가고 -3은 넘어감 - 끝 숫자는 포함하지 않으니까)
    print(tplTest[j])
print(tplTest[1:3]) # (2 3) 여전히 튜플이다
print(tplTest+tplTest) # (1,2,3,1,2,3)
print(tplTest*3) # (1,2,3,1,2,3,1,2,3)

#TypeError: 'tuple' object does not support item assignment 에러 발생함. assign하는것 불가
#상수를 구현하고 싶을 때 사용함. 고정된 값을 쓸 때 사용
#tplTest[0] = 100